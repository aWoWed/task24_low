﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Controllers
{
    public class GuestController : Controller
    {
        public ActionResult Index()
        {
            return View(HttpContext.Application["comments"]);
        }

        [HttpPost]
        public ActionResult AddComment(Comment comment)
        {
            if (string.IsNullOrEmpty(comment.Name))
                ModelState.AddModelError(string.Empty, "Error! Please enter your name!");

            if (string.IsNullOrEmpty(comment.Text))
                ModelState.AddModelError(string.Empty, "Error! Please enter your text!");

            if (ModelState.IsValid)
            {
                (HttpContext.Application["comments"] as List<Comment>)?.Add(comment);
                return RedirectToAction("Index");
            }

            return View("Index", HttpContext.Application["comments"]);
        }
    }
}
