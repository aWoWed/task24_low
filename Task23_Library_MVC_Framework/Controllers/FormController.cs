﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Task23_Library_MVC_Framework.Models;

namespace Task23_Library_MVC_Framework.Controllers
{
    public class FormController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Result(FormAnswer formAnswer, IList<string> genres)
        {
            if (string.IsNullOrEmpty(formAnswer.Name))
                ModelState.AddModelError("Name","Please enter your name");

            if (string.IsNullOrEmpty(formAnswer.Genres))
                ModelState.AddModelError("Genres", "Select at least one of the genres");

            if (string.IsNullOrEmpty(formAnswer.ReadingPassion))
                ModelState.AddModelError("ReadingPassion", "Please choice your reading passion");

            foreach (var genre in genres)
            {
                if(!formAnswer.Genres.Contains(genre))
                    formAnswer.Genres += " " + genre;
            }

            if (!ModelState.IsValid) return View("Index");
            (HttpContext.Application["forms"] as List<FormAnswer>)?.Add(formAnswer);
            return RedirectToAction("Result");
        }

        [HttpGet]
        public ActionResult Result()
        {
            return View("Result", HttpContext.Application["forms"]);
        }
    }
}
