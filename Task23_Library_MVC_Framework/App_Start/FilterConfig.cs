﻿using System.Web;
using System.Web.Mvc;

namespace Task23_Library_MVC_Framework
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
