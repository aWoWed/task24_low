﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task23_Library_MVC_Framework.Models
{
    public abstract class BaseModel
    {
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string Text { get; set; }

        protected BaseModel()
        {
            CreationDate = DateTime.Now;
        }
    }
}
